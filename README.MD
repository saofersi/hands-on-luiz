## Hands-on fit
---

Você tem uma imagem que representa uma planta baixa. Você deve plotar nessa planta baixa ícones que representem pessoas. Esses ícones devem ser atualizados a cada 10s, refletindo as alterações de posição. Ao clicar em um ícone, mostrar num lightbox informações referentes a essa pessoa (nome e coordenadas da localização) e uma caixa de texto para enviar uma mensagem a ela. As mensagens enviadas devem ser mostradas no console.As imagens que você vai precisar estão disponíveis em /images.

Exemplo:

![alt text](images/sample.png)

 Utilizar JSON sobre REST, HTML5, CSS3 e JS com JQuery. O resto das tecnologias podem ser escolhidas por você.